﻿Imports mbapix.mbapix
Imports mbapix.mbapix.ServiceFacade

Module mbapixconsole
#Region "Fields"
    Private _basicService As BasicServiceFacade

#End Region

    Public Sub Main()
#Region "Login Scope"
        ' Hook up the session id update        
        AddHandler SessionManager.INSTANCE.OnSessionIdChanged, AddressOf HandleSessionIdChanged

        ' Initialize the service facades
        _basicService = New BasicServiceFacade(My.Settings.BasicServiceUrl)
#End Region
    End Sub

    Private Sub FleetLogin()

        Try
            LogStart("Login")
            Dim sessionId = _basicService.Login(My.Settings.Fleet, My.Settings.Username, My.Settings.Password)
            LogEnd(sessionId)

            If String.IsNullOrEmpty(sessionId) Then
                LogEnd("Login failed")
            Else
                Dim properties = _basicService.GetServerProperties().ToList()
                '_tmService.ApplyServerProperties(properties)
                '_posService.ApplyServerProperties(properties)
                '_importPosReports.ApplyServerProperties(properties)
            End If

        Catch ex As FleetboardFaultException

            Select Case ex.Details.ErrorCode
                Case FleetboardFaultException.ERROR_INVALID_CREDENTIALS
                    LogEnd("Login failed: Invalid credentials (Error " & ex.Details.ErrorCode & ")")
                Case Else
                    LogEnd("Login failed: " & ex.Details.Message & " (Error " + ex.Details.ErrorCode & ")")
            End Select

        Catch ex As Exception
            LogEnd("Login failed: " & ex.Message)
        End Try
    End Sub

    Private Sub HandleSessionIdChanged(ByVal sender As Object, ByVal sessionIdEventArgs As SessionIdEventArgs)
        'If InvokeRequired Then
        '    Invoke(New Action(Of Object, SessionIdEventArgs)(AddressOf HandleSessionIdChanged), sender, sessionIdEventArgs)
        '    Return
        'End If

        If String.IsNullOrEmpty(sessionIdEventArgs.SessionId) Then
            Console.WriteLine("Not logged in")
        Else
            Console.WriteLine("Logged in with " & sessionIdEventArgs.SessionId)
        End If
    End Sub


    Private Sub LogStart(ByVal actionName As String)
        Console.WriteLine("Running...")
    End Sub
    Private Sub LogEnd(ByVal result As String)
        Console.WriteLine(result)
    End Sub

End Module
