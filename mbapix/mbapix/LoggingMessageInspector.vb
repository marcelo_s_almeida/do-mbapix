﻿Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Dispatcher

Namespace mbapix
    Public Class LoggingMessageInspector
        Implements IClientMessageInspector

        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger("SoapLog")
        Private Shared ReadOnly _INSTANCE As LoggingMessageInspector = New LoggingMessageInspector()

        Public Shared ReadOnly Property INSTANCE As LoggingMessageInspector
            Get
                Return _INSTANCE
            End Get
        End Property

        Private Sub New()
        End Sub

        Public Sub AfterReceiveReply(ByRef reply As Message, ByVal correlationState As Object)
            log.Info(reply.ToString())
        End Sub

        Public Function BeforeSendRequest(ByRef request As Message, ByVal channel As IClientChannel) As Object
            log.Info(request.ToString())
            Return Nothing
        End Function

        Private Function IClientMessageInspector_BeforeSendRequest(ByRef request As Message, channel As IClientChannel) As Object Implements IClientMessageInspector.BeforeSendRequest
            Return DirectCast(INSTANCE, IClientMessageInspector).BeforeSendRequest(request, channel)
        End Function

        Private Sub IClientMessageInspector_AfterReceiveReply(ByRef reply As Message, correlationState As Object) Implements IClientMessageInspector.AfterReceiveReply
            DirectCast(INSTANCE, IClientMessageInspector).AfterReceiveReply(reply, correlationState)
        End Sub
    End Class
End Namespace
