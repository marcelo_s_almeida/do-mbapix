﻿Imports System
Imports System.Runtime.Serialization
Imports System.ServiceModel

Namespace mbapix.ServiceFacade
    <Serializable>
    Public Class FleetboardFaultException
        Inherits Exception

        Private ReadOnly _details As FaultDetails
        Public Const ERROR_INVALID_CREDENTIALS As Integer = 1007002

        Public ReadOnly Property Details As FaultDetails
            Get
                Return _details
            End Get
        End Property

        Public Sub New(ByVal details As FaultDetails, ByVal faultException As FaultException)
            MyBase.New(details.Message, faultException)
            _details = details
        End Sub

        Public Shared Function Create(ByVal faultException As FaultException) As FleetboardFaultException
            Dim msgFault = faultException.CreateMessageFault()
            If Not msgFault.HasDetail Then Return Nothing
            Dim details = msgFault.GetDetail(Of FaultDetails)()
            Return New FleetboardFaultException(details, faultException)
        End Function

        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class
End Namespace
