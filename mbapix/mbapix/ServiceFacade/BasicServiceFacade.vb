﻿Imports System
Imports System.Collections.Generic
Imports System.ServiceModel
Imports mbapix.BasicService

Namespace mbapix.ServiceFacade
    Public Class BasicServiceFacade
        Private _basicService As BasicService.BasicService

        Public Sub New(ByVal webserviceUrl As String)
            _basicService = ClientFactory.CreateClient(Of BasicService.BasicService)(webserviceUrl)

            SessionManager.INSTANCE.OnSessionIdChanged += Sub(ByVal sender As Object, ByVal args As SessionIdEventArgs)
                                                              _basicService = ClientFactory.CreateClient(Of BasicService.BasicService)(webserviceUrl, args.SessionId)
                                                          End Sub
        End Sub

        Public Function Login(ByVal fleet As String, ByVal username As String, ByVal password As String) As String
            ServiceLog.LogStart("BasicService", "Login", "Fleetname: {0}, User: {1}", fleet, username)

            Try
                Dim response = _basicService.login(New loginRequest1(New login() With {
                    .LoginRequest = New LoginRequest() With {
                        .Fleetname = fleet,
                        .User = username,
                        .Password = password
                    }
                }))
                Dim sessionId = response.loginResponse.LoginResponse.sessionid
                ServiceLog.LogEnd("BasicService", "Login", "SessionId: {0}", sessionId)
                SessionManager.INSTANCE.UpdateSessionId(sessionId)
                Return sessionId
            Catch ex As FaultException
                Dim fleetboardException = FleetboardFaultException.Create(ex)

                If fleetboardException IsNot Nothing Then
                    Throw fleetboardException
                Else
                    Throw
                End If
            End Try
        End Function

        Public Function Logout() As Boolean
            ServiceLog.LogStart("BasicService", "Logout", "")
            Dim response = _basicService.logout(New logoutRequest1(New logout() With {
                .LogoutRequest = New LogoutRequest()
            }))
            ServiceLog.LogEnd("BasicService", "Logout", "")
            SessionManager.INSTANCE.UpdateSessionId(Nothing)
            Return response.logoutResponse.LogoutResponse.OK IsNot Nothing
        End Function

        Public Function GetCurrentUser() As TmSoapUserType
            ServiceLog.LogStart("BasicService", "GetCurrentUser", "")
            Dim response = _basicService.getCurrentUser(New getCurrentUserRequest1(New getCurrentUser() With {
                .GetCurrentUserRequest = New GetCurrentUserRequest() With {
                    .version = 10500,
                    .versionSpecified = True
                }
            }))
            ServiceLog.LogEnd("BasicService", "GetCurrentUser", "")
            Dim user = response.getCurrentUserResponse.GetCurrentUserResponse.User
            Return user
        End Function

        Public Function ForceRelogin(ByVal fleet As String, ByVal username As String, ByVal password As String) As String
            SessionManager.INSTANCE.UpdateSessionId(Nothing)
            Return Login(fleet, username, password)
        End Function

        Public Function GetVehicles() As IEnumerable(Of VEHICLE)
            Dim request = New VehicleRequest(_basicService, New getVehicleRequest1(New getVehicle() With {
                .GetVehicleRequest = New GetVehicleRequest() With {
                    .version = 10500,
                    .versionSpecified = True
                }
            }))
            Return request.GetAllResults()
        End Function

        Public Function GetDrivers() As IEnumerable(Of GetDriverResponseTypeDriverInfo)
            Dim request = New DriverRequest(_basicService, New getDriverRequest(New getDriver() With {
                .GetDriverRequest = New GetDriverRequestType() With {
                    .Status = driverStatusType.ACTIVE,
                    .StatusSpecified = True,
                    .version = 10500,
                    .versionSpecified = True
                }
            }))
            Return request.GetAllResults()
        End Function

        Public Function GetServerProperties(ParamArray properties As GetServerPropertiesRequestProperty()) As IEnumerable(Of PropertyType)
            Dim request = New GetServerPropertiesRequest(_basicService, New getServerPropertiesRequest1(New getServerProperties() With {
                .GetServerPropertiesRequest = New BasicService.GetServerPropertiesRequest() With {
                    .[Property] = properties
                }
            }))
            Return request.GetAllResults()
        End Function

        Private Class VehicleRequest
            Inherits CursoringRequestBase(Of VEHICLE)

            Private ReadOnly _basicService As BasicService.BasicService
            Private ReadOnly _request As getVehicleRequest1
            Private _response As getVehicleResponse1

            Public Sub New(ByVal basicService As BasicService.BasicService, ByVal request As getVehicleRequest1)
                _basicService = basicService
                _request = request
            End Sub

            Protected Overrides Sub SetDate(ByVal begin As DateTime, ByVal [end] As DateTime)
            End Sub

            Protected Overrides Sub Execute()
                ServiceLog.LogStart("BasicService", "GetVehicle", "Offset: {0}, Limit: {1}", _request.getVehicle.GetVehicleRequest.offset, _request.getVehicle.GetVehicleRequest.limit)
                _response = _basicService.getVehicle(_request)
                ServiceLog.LogEnd("BasicService", "GetVehicle", "ResultSize: {0}, ResponseSize: {1}", _response.getVehicleResponse.GetVehicleResponse.resultSize, _response.getVehicleResponse.GetVehicleResponse.responseSize)
            End Sub

            Protected Overrides Sub SetOffsetAndLimit(ByVal offset As String, ByVal limit As String)
                _request.getVehicle.GetVehicleRequest.offset = offset
                _request.getVehicle.GetVehicleRequest.limit = limit
            End Sub

            Protected Overrides Function GetResult() As IEnumerable(Of VEHICLE)
                Return _response.getVehicleResponse.GetVehicleResponse.VEHICLE
            End Function

            Protected Overrides Function GetResponseSize() As String
                Return _response.getVehicleResponse.GetVehicleResponse.responseSize
            End Function

            Protected Overrides Function GetResultSize() As String
                Return _response.getVehicleResponse.GetVehicleResponse.resultSize
            End Function

            Protected Overrides Function GetResponseLimit() As String
                Return _response.getVehicleResponse.GetVehicleResponse.limit
            End Function
        End Class

        Private Class DriverRequest
            Inherits CursoringRequestBase(Of GetDriverResponseTypeDriverInfo)

            Private ReadOnly _basicService As BasicService.BasicService
            Private ReadOnly _request As getDriverRequest
            Private _response As getDriverResponse1

            Public Sub New(ByVal basicService As BasicService.BasicService, ByVal request As getDriverRequest)
                _basicService = basicService
                _request = request
            End Sub

            Protected Overrides Sub SetDate(ByVal begin As DateTime, ByVal [end] As DateTime)
            End Sub

            Protected Overrides Sub Execute()
                ServiceLog.LogStart("BasicService", "GetDriver", "Offset: {0}, Limit: {1}", _request.getDriver.GetDriverRequest.offset, _request.getDriver.GetDriverRequest.limit)
                _response = _basicService.getDriver(_request)
                ServiceLog.LogEnd("BasicService", "GetDriver", "ResultSize: {0}, ResponseSize: {1}", _response.getDriverResponse.GetDriverResponse.resultSize, _response.getDriverResponse.GetDriverResponse.responseSize)
            End Sub

            Protected Overrides Sub SetOffsetAndLimit(ByVal offset As String, ByVal limit As String)
                _request.getDriver.GetDriverRequest.offset = offset
                _request.getDriver.GetDriverRequest.limit = limit
            End Sub

            Protected Overrides Function GetResult() As IEnumerable(Of GetDriverResponseTypeDriverInfo)
                Return _response.getDriverResponse.GetDriverResponse.DriverInfo
            End Function

            Protected Overrides Function GetResponseSize() As String
                Return _response.getDriverResponse.GetDriverResponse.responseSize
            End Function

            Protected Overrides Function GetResultSize() As String
                Return _response.getDriverResponse.GetDriverResponse.resultSize
            End Function

            Protected Overrides Function GetResponseLimit() As String
                Return _response.getDriverResponse.GetDriverResponse.limit
            End Function
        End Class

        Private Class GetServerPropertiesRequest
            Inherits CursoringRequestBase(Of PropertyType)

            Private ReadOnly _service As BasicService.BasicService
            Private ReadOnly _request As getServerPropertiesRequest1
            Private _response As getServerPropertiesResponse1

            Public Sub New(ByVal service As BasicService.BasicService, ByVal request As getServerPropertiesRequest1)
                _service = service
                _request = request
            End Sub

            Protected Overrides Sub SetOffsetAndLimit(ByVal offset As String, ByVal limit As String)
                _request.getServerProperties.GetServerPropertiesRequest.offset = offset
                _request.getServerProperties.GetServerPropertiesRequest.limit = limit
            End Sub

            Protected Overrides Function GetResult() As IEnumerable(Of PropertyType)
                Return _response.getServerPropertiesResponse.GetServerPropertiesResponse.[Property]
            End Function

            Protected Overrides Function GetResponseSize() As String
                Return _response.getServerPropertiesResponse.GetServerPropertiesResponse.responseSize
            End Function

            Protected Overrides Function GetResponseLimit() As String
                Return _response.getServerPropertiesResponse.GetServerPropertiesResponse.limit
            End Function

            Protected Overrides Function GetResultSize() As String
                Return _response.getServerPropertiesResponse.GetServerPropertiesResponse.resultSize
            End Function

            Protected Overrides Sub SetDate(ByVal begin As DateTime, ByVal [end] As DateTime)
            End Sub

            Protected Overrides Sub Execute()
                ServiceLog.LogStart("BasicService", "GetServerProperties", "Offset: {0}, Limit: {1}", _request.getServerProperties.GetServerPropertiesRequest.offset, _request.getServerProperties.GetServerPropertiesRequest.limit)
                _response = _service.getServerProperties(_request)
                ServiceLog.LogEnd("BasicService", "GetServerProperties", "ResultSize: {0}, ResponseSize: {1}", _response.getServerPropertiesResponse.GetServerPropertiesResponse.resultSize, _response.getServerPropertiesResponse.GetServerPropertiesResponse.responseSize)
            End Sub
        End Class
    End Class
End Namespace
