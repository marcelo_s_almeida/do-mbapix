﻿Imports System.Runtime.Serialization

Namespace mbapix.ServiceFacade
    ' <summary>
    ' Fleetboard specific SOAP errors.
    ' </summary>
    <DataContract(Name:="FbFaultDetails", [Namespace]:="http://www.fleetboard.com/data")>
    Public Class FaultDetails
        ' <summary>
        ' The error code. See http://webservices.fleetboard.com/de/verwendung_von_sessions
        ' </summary>
        <DataMember(Name:="errorcode")>
        Public Property ErrorCode As Integer

        ' <summary>
        ' The error message.
        ' </summary>
        <DataMember(Name:="message")>
        Public Property Message As String
    End Class
End Namespace
