﻿Imports System
Imports System.Collections.Generic
Imports System.Globalization
Imports System.Linq

Namespace mbapix.ServiceFacade
    MustInherit Class CursoringRequestBase(Of T)
        Public Function GetSingleResult() As T
            SetOffsetAndLimit("0", "1")
            Execute()
            Dim result = GetResult()
            If result Is Nothing Then Throw New Exception("No result")
            Return result.FirstOrDefault()
        End Function

        Public Iterator Function GetAllResults() As IEnumerable(Of T)
            Dim offset As Integer = 0
            Dim limit As Integer? = Nothing
            Dim receivedItems As Integer = 0

            While True
                Dim offsetString As String = offset.ToString(CultureInfo.InvariantCulture)
                Dim limitString As String = If(limit.HasValue, limit.Value.ToString(CultureInfo.InvariantCulture), Nothing)
                SetOffsetAndLimit(offsetString, limitString)
                Execute()
                Dim responseSizeString As String = GetResponseSize()
                Dim responseSize As Integer
                Dim responseSizeParsed As Boolean = Integer.TryParse(responseSizeString, responseSize)
                If Not responseSizeParsed OrElse responseSize = 0 Then Return
                Dim resultSizeString As String = GetResultSize()
                Dim resultSize As Integer
                Dim resultSizeParsed As Boolean = Integer.TryParse(resultSizeString, resultSize)
                If Not resultSizeParsed Then Return
                Dim responseLimitString As String = GetResponseLimit()
                Dim responseLimit As Integer
                Dim limitParsed As Boolean = Integer.TryParse(responseLimitString, responseLimit)
                If Not limitParsed Then Return
                limit = responseLimit

                For Each result In GetResult()
                    receivedItems += 1
                    Yield result
                Next

                If receivedItems >= resultSize Then Return
                offset += responseSize
            End While
        End Function

        Public Iterator Function GetAllResultsWithDateRange(ByVal begin As DateTime, ByVal [end] As DateTime, ByVal maxSpan As TimeSpan) As IEnumerable(Of T)
            Dim dateRanges = SplitDateRange(begin, [end], maxSpan)

            For Each dateRange In dateRanges
                SetDate(dateRange.Item1, dateRange.Item2)

                For Each result In GetAllResults()
                    Yield result
                Next
            Next
        End Function

        Protected MustOverride Sub SetOffsetAndLimit(ByVal offset As String, ByVal limit As String)
        Protected MustOverride Function GetResult() As IEnumerable(Of T)
        Protected MustOverride Function GetResponseSize() As String
        Protected MustOverride Function GetResponseLimit() As String
        Protected MustOverride Function GetResultSize() As String
        Protected MustOverride Sub SetDate(ByVal begin As DateTime, ByVal [end] As DateTime)
        Protected MustOverride Sub Execute()

        Private Iterator Function SplitDateRange(ByVal begin As DateTime, ByVal [end] As DateTime, ByVal maxSpan As TimeSpan) As IEnumerable(Of Tuple(Of DateTime, DateTime))
            If maxSpan < TimeSpan.Zero Then
                Yield New Tuple(Of DateTime, DateTime)(begin, [end])
            Else
                Dim current = begin

                While current + maxSpan < [end]
                    Yield New Tuple(Of DateTime, DateTime)(current, current + maxSpan)
                    current += maxSpan
                End While

                Yield New Tuple(Of DateTime, DateTime)(current, [end])
            End If
        End Function
    End Class
End Namespace
