﻿Imports System

Namespace mbapix.ServiceFacade
    Public Class SessionManager
#Region "Log"
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#End Region

#Region "Singleton Pattern"
        Private Shared ReadOnly _INSTANCE As SessionManager = New SessionManager()

        Public Shared ReadOnly Property INSTANCE As SessionManager
            Get
                Return _INSTANCE
            End Get
        End Property
#End Region

#Region "Fields"
        Private _sessionId As String
#End Region

#Region "Properties"
        Public ReadOnly Property SessionId As String
            Get
                Return _sessionId
            End Get
        End Property
#End Region

#Region "Events"
        Public Event OnSessionIdChanged As EventHandler(Of SessionIdEventArgs)
#End Region

#Region "Constructor"
        Private Sub New()
        End Sub
#End Region

#Region "Methods"
        Public Sub UpdateSessionId(ByVal sessionId As String)
            log.InfoFormat("Updated session id: '{0}'", sessionId)
            _sessionId = sessionId
            'Dim tmp As OnSessionIdChanged
            RaiseEvent OnSessionIdChanged(Me, New SessionIdEventArgs(sessionId))
        End Sub
#End Region
    End Class
End Namespace
