﻿Namespace mbapix
    '/// <summary>
    '/// Helper class for logging service invocations.
    '/// </summary>
    Module ServiceLog
#Region "Log"
        Private ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#End Region
        '/// <summary>
        '/// Logs the invocation of a web service.
        '/// </summary>
        '/// <param name="service">The name of the service.</param>
        '/// <param name="operation">The name of the operation.</param>
        '/// <param name="format">The parameter's format string.</param>
        '/// <param name="args">The parameter values.</param>
        Sub LogStart(ByVal service As String, ByVal operation As String, ByVal format As String, ParamArray args As Object())
            Dim argumentString As String = String.Format(format, args)
            Dim argumentSeparator As String = If(argumentString = "", "", ", ")
            log.InfoFormat("-> {0}.{1}(Session: {2}{3}{4})", service, operation, ServiceFacade.SessionManager.INSTANCE.SessionId, argumentSeparator, argumentString)
        End Sub

        '/// <summary>
        '/// Logs the return of a web service invocation.
        '/// </summary>
        '/// <param name="service">The name of the service.</param>
        '/// <param name="operation">The name of the operation.</param>
        '/// <param name="format">The return value's format string.</param>
        '/// <param name="args">The return values.</param>
        Sub LogEnd(ByVal service As String, ByVal operation As String, ByVal format As String, ParamArray args As Object())
            Dim resultString As String = String.Format(format, args)
            log.InfoFormat("<- {0}.{1}(Session: {2}) {3}", service, operation, ServiceFacade.SessionManager.INSTANCE.SessionId, resultString)
        End Sub
    End Module
End Namespace
