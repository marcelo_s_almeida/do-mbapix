﻿Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Description
Imports System.ServiceModel.Dispatcher

Namespace mbapix
    Module ClientFactory
        Private ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Function CreateClient(Of T)(ByVal url As String) As T
            Dim binding = New BasicHttpBinding With {
                .AllowCookies = False,
                .MaxReceivedMessageSize = 10 * 1024 * 1024
            }
            Dim factory = New ChannelFactory(Of T)(binding, New EndpointAddress(url))
            factory.Endpoint.Behaviors.Add(New EndpointBehavior())
            Return factory.CreateChannel()
        End Function

        Function CreateClient(Of T)(ByVal url As String, ByVal sessionId As String) As T
            Return CreateClient(Of T)(url & ";jsessionid=" & sessionId)
        End Function

        Private Class EndpointBehavior
            Implements IEndpointBehavior

            'Public Sub AddBindingParameters(ByVal endpoint As ServiceEndpoint, ByVal bindingParameters As BindingParameterCollection)
            'End Sub

            'Public Sub ApplyClientBehavior(ByVal endpoint As ServiceEndpoint, ByVal clientRuntime As ClientRuntime)
            '    clientRuntime.MessageInspectors.Add(LoggingMessageInspector.INSTANCE)
            'End Sub

            'Public Sub ApplyDispatchBehavior(ByVal endpoint As ServiceEndpoint, ByVal endpointDispatcher As EndpointDispatcher)
            'End Sub

            'Public Sub Validate(ByVal endpoint As ServiceEndpoint)
            'End Sub

            Private Sub IEndpointBehavior_Validate(endpoint As ServiceEndpoint) Implements IEndpointBehavior.Validate
                Throw New NotImplementedException()
            End Sub

            Private Sub IEndpointBehavior_AddBindingParameters(endpoint As ServiceEndpoint, bindingParameters As BindingParameterCollection) Implements IEndpointBehavior.AddBindingParameters
                Throw New NotImplementedException()
            End Sub

            Private Sub IEndpointBehavior_ApplyDispatchBehavior(endpoint As ServiceEndpoint, endpointDispatcher As EndpointDispatcher) Implements IEndpointBehavior.ApplyDispatchBehavior
                Throw New NotImplementedException()
            End Sub

            Private Sub IEndpointBehavior_ApplyClientBehavior(endpoint As ServiceEndpoint, clientRuntime As ClientRuntime) Implements IEndpointBehavior.ApplyClientBehavior
                'Throw New NotImplementedException()
                clientRuntime.MessageInspectors.Add(LoggingMessageInspector.INSTANCE)
            End Sub
        End Class
    End Module
End Namespace
