﻿Namespace mbapix
    '/// <summary>
    '/// Helper class to keep constants for semantic ids.
    '/// </summary>
    Module SemanticId
        '/// <summary>
        '/// Magic semantic constant for transport objects.
        '/// </summary>
        Public Const TRANSPORT_OBJECT As Integer = 233

        '/// <summary>
        '/// Magic semantic constant for activities.
        '/// </summary>
        Public Const ACTIVITY As Integer = 234

        '/// <summary>
        '/// Magic semantic constant for events.
        '/// </summary>
        Public Const [EVENT] As Integer = 235

        '/// <summary>
        '/// Magic semantic constant for messages.
        '/// </summary>
        Public Const MESSAGE As Integer = 236

        '/// <summary>
        '/// Semantic constant for fields containing the KM reading.
        '/// </summary>
        Public Const KM_READING As Integer = 150

        '/// <summary>
        '/// Semantic constant for fields containing the total fuel used.
        '/// </summary>
        Public Const TOTAL_FUEL_USED As Integer = 153

        '/// <summary>
        '/// Semantic constant for fields containing the fuel level.
        '/// </summary>
        Public Const FUEL_LEVEL As Integer = 154

        '/// <summary>
        '/// Semantic constant for fields containing the name of driver 1.
        '/// </summary>
        Public Const DRIVER_NAME_ID_1 As Integer = 158

        '/// <summary>
        '/// Semantic constant for fields containing the name of driver 2.
        '/// </summary>
        Public Const DRIVER_NAME_ID_2 As Integer = 159

        '/// <summary>
        '/// Semantic constant for fields containing the longitude.
        '/// </summary>
        Public Const LONGITUDE As Integer = 160

        '/// <summary>
        '/// Semantic constant for fields containing the latitude.
        '/// </summary>
        Public Const LATITUDE As Integer = 161

        '/// <summary>
        '/// Semantic constant for fields containing the activties begin timestamp.
        '/// </summary>
        Public Const ACTIVITY_BEGIN_TIMESTAMP As Integer = 192

        '/// <summary>
        '/// Semantic constant for fields containing the activties end timestamp.
        '/// </summary>
        Public Const ACTIVITY_END_TIMESTAMP As Integer = 193

        '/// <summary>
        '/// Semantic constant for fields containing the event timestamp.
        '/// </summary>
        Public Const EVENT_TIMESTAMP As Integer = 194
    End Module
End Namespace
